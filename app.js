var express = require('express');
var db = require('./db');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var fs = require('fs');
var session = require('express-session');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var flash = require('connect-flash');

var app = express();

app.set('trust proxy', 1);
app.use(session({
  secret: '!gasd!pG$hsaf%',
  resave: false,
  saveUninitialized: true,
}));


// view engine setup
app.set('views', path.join(__dirname, '/views'));
app.set('view engine', 'jade');
app.use(express.static(path.join(__dirname, '/public')));

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

// Passport init
app.use(passport.initialize());
app.use(passport.session());

app.use(flash());

// global vars
app.use(function (req, res, next) {
  res.locals.user = req.user || null;
  next();
});

// include routes from controllers
var index = require('./routes/index');
var users = require('./routes/users');
var boards = require('./routes/boards');
var sprints = require('./routes/sprints');
var tickets = require('./routes/tickets');

app.use('/', index);
app.use('/', users);
app.use('/boards', boards);
app.use('/sprints', sprints);
app.use('/tickets', tickets);

//connect to MySQL
db.connect(db.MODE_PRODUCTION, function(err) {
  if (err) {
    console.log('Unable to connect to MySQL.');
    process.exit(1);
  }
});

module.exports = app;
