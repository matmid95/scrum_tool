exports.checkAuth = function checkAuth(req, res, next) {

  if(req.isAuthenticated()) {
    return next();
  } else {
    res.status(400).redirect('/login');
  }
}
