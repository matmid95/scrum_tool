var db = require('../db');
var model = require('nodejs-model');

var Ticket = model("Ticket").attr('name', {
  validations: {
    presence: {
      message: 'Name is required.'
    }
  }
}).attr('creator', {
  validations: {
    presence: {
      message: 'Creator is required.'
    }
  }
}).attr('description', {
  validations: {
    presence: {
      message: 'Creator is required.'
    }
  }
}).attr('storyPoints', {
}).attr('assignee', {
}).attr('createdAt', {
}).attr('boardId', {
  validations: {
    presence: {
      message: 'Creator is required.'
    }
  }
});

Ticket.save = function(ticket, done) {
  var assignee = null;
  var createdAt = new Date().toISOString();

  var values = [ticket.name(), ticket.creator(), ticket.description(), assignee, createdAt, ticket.boardId(), ticket.storyPoints()];

  db.get().query('INSERT INTO tickets (title, creator, description, assignee, created_at, board_id, estimate, status_id)' +
                 ' VALUES (?, ?, ?, ?, ?, ?, ?, 1)', values,
                 function(err, result) {
                   if (err) return done(err);
                   done(null, result.insertId);
                 });

}

Ticket.updateStatus = function(ticketId, ticketStatus, done) {
  var values = [ticketStatus, ticketId];

  db.get().query('UPDATE tickets SET status_id = ? WHERE id = ?', values, function(err, result){
    if (err) return done(err);
    done(null, result);
  });
}

Ticket.getTicketFromId = function(ticketId, done) {
  db.get().query('SELECT * FROM tickets WHERE id = ?',
      ticketId,
      function(err, results) {
        if (err) return done(err);
        done(null, results[0]);
      });
}

Ticket.getTicketsForBoard = function(boardId, done) {
  db.get().query('SELECT * FROM tickets WHERE board_id = ? ORDER BY position DESC',
      boardId,
      function(err, results) {
        if (err) return done(err);
        done(null, results);
      });
}

Ticket.updatePosition = function (newPos, ticketId, done) {
  db.get().query('UPDATE tickets SET position = ? WHERE id = ?',
      [newPos, ticketId],
      function(err, results) {
        if (err) return done(err);
        done(null, results);
      });
}

Ticket.assignUser = function (userId, ticketId, done) {
  db.get().query('UPDATE tickets SET assignee = ? WHERE id = ?',
      [userId, ticketId],
      function(err, results) {
        if (err) return done(err);
        done(null, results);
      });
}

Ticket.getAssignee = function(ticketId, done) {
  db.get().query('SELECT * FROM users INNER JOIN tickets ON tickets.assignee = users.id WHERE tickets.id = ?',
      ticketId,
      function(err, results) {
        if (err) return done(err);
        done(null, results[0]);
      });
}

exports.Ticket = Ticket;
