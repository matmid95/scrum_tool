var db = require('../db');
var model = require('nodejs-model');
var bcrypt = require('bcryptjs');

var User = model("User").attr('name', {
  validations: {
    presence: {
      message: 'Name is required.'
    },
  }
}).attr('surname', {
  validations: {
    presence: {
      message: 'Surname is required.'
    }
  }
}).attr('email', {
  validations: {
    presence: {
      message: 'Email is required.'
    },
    uniqueEmail: {
      message: 'An account already exists from the email you entered.'
    }
  }
}).attr('password', {
  validations: {
    presence: {
      message: 'Password is required.'
    },
    length: {
      minimum: 6,
      maximum: 25,
      messages: { tooShort: 'Password must be between 6 and 25 characters', tooLong: 'Password must be between 6 and 25 characters'}
    }
  }
});

User.validator('uniqueEmail', function(model, property, options) {
  exists(model.email(), function(err, userExists) {
    if (userExists) {
      model.addError(property, options.message);
    }
  });

});

User.save = function(user, done) {
  var password = user.password();

  bcrypt.genSalt(10, function(err, salt) {
    bcrypt.hash(password, salt, function(err, hash) {
      var values = [user.name(), user.surname(), user.email(),
                    hash];

      db.get().query('INSERT INTO users (name, surname, email,' +
                     ' password)' +
                     ' VALUES (?, ?, ?, ?)', values,
                     function(err, result) {
                       if (err) return done(err);
                       done(null, result.insertId);
                     });
    });
  });
}

function exists(email, done) {
  db.get().query('SELECT * FROM users WHERE email = ?', [email],
                function (err, user) {
                   if (err) return done(err);
                   if (user.length <= 1) {
                     done(null, true);
                   }
                   else {
                     done(null, false);
                   }
                 });
}

User.getUserByEmail = function(email, done){
  db.get().query('SELECT * FROM users WHERE email = ?', [email],
                function (err, rows) {
                  if (err) return done(err);
                  done(null, rows[0]);
                });
}


User.getUserById = function(id, done){
  db.get().query('SELECT * FROM users WHERE id = ?', [id],
                function (err, user) {
                   if (err) return done(err);
                   done(null, user[0]);
                 });
}

User.comparePassword = function(password, hash, done) {
  bcrypt.compare(password, hash, function(err, isMatch) {
    if(err) throw err;
    done(null, isMatch);
  });
}

User.getAllEmails = function(done) {
  db.get().query('SELECT email FROM users', null,
                 function (err, rows) {
                   if (err) return done(err, null);
                   done(null, rows);
                 });
}

// private methods


exports.User = User;
