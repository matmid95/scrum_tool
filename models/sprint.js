var db = require('../db');
var model = require('nodejs-model');
var dateFormat = require('dateformat');

var Sprint = model("Sprint").attr('title', {
  validations: {
    presence: {
      message: 'Title is required.'
    }
  }
}).attr('storyPoints', {
}).attr('remainingStoryPoints', {
}).attr('startDate', {
}).attr('endDate', {
});

Sprint.save = function(sprint, done) {
  var values = [sprint.title(), sprint.storyPoints(), sprint.remainingStoryPoints(), sprint.startDate(), sprint.endDate()]

  db.get().query('INSERT INTO sprints (title, story_points, remaining_story_points, start_date, end_date)' +
                 ' VALUES (?, ?, ?, ?, ?)', values,
                 function(err, result) {
                   if (err) return done(err);
                   done(null, result.insertId);
                 });

}

Sprint.getSprint = function(sprintId, done) {
  var values = [sprintId];

  db.get().query('SELECT * FROM sprints WHERE id = ?', values, function(err, result) {
    if (err) return done(err);
    result[0].start_date = formatDate(result[0].start_date);
    result[0].end_date = formatDate(result[0].end_date);
    done(null, result[0]);
  });
}

function formatDate(date) {
  return dateFormat(date, "longDate");
}

Sprint.getTickets = function(sprintId, done) {
  var values = [sprintId];

  db.get().query('SELECT tickets.* FROM tickets INNER JOIN sprinttickets ON sprinttickets.ticket_id = tickets.id INNER JOIN sprints ON sprints.id = sprinttickets.sprint_id WHERE sprints.id = ?', values, function(err, result) {
    if (err) return done(err);
    done(null, result);
  });
}

Sprint.getBoard = function(sprintId, done) {
  var values = [sprintId];

  db.get().query('SELECT boards.* FROM sprints INNER JOIN sprinttickets ON sprints.id = sprinttickets.sprint_id INNER JOIN tickets ON sprinttickets.ticket_id = tickets.id INNER JOIN boards ON tickets.board_id = boards.id WHERE sprint_id = ?', values,
                 function(err, result) {
                   if (err) return done(err);
                   done(null, result[0]);
                 });

}

Sprint.populateSprintTickets = function(sprintId, ticketId, done) {
  var values = [sprintId, ticketId];

  db.get().query('INSERT INTO sprinttickets (sprint_id, ticket_id)' +
                 ' VALUES (?, ?)', values,
                 function(err, result) {
                   if (err) return done(err);
                   done(null, result.insertId);
                 });

}

Sprint.calculateStoryPoints = function(sprintId, done){
  values = [sprintId];

  db.get().query('SELECT SUM(tickets.estimate) AS total_story_points FROM sprints INNER JOIN sprinttickets ON sprinttickets.sprint_id = sprints.id INNER JOIN tickets ON sprinttickets.ticket_id = tickets.id WHERE sprints.id = ?', values, function(err, result){
    if (err) return done(err);
    var storyPoints = result[0].total_story_points;
    done(null, storyPoints);
  });
}

Sprint.calculateRemainingStoryPoints = function(sprintId, done){
  values = [sprintId];

  db.get().query('SELECT SUM(tickets.estimate) AS total_story_points FROM sprints INNER JOIN sprinttickets ON sprinttickets.sprint_id = sprints.id INNER JOIN tickets ON sprinttickets.ticket_id = tickets.id WHERE sprints.id = ? AND tickets.status_id <> 4', values, function(err, result){
    if (err) return done(err);
    var storyPoints = result[0].total_story_points;
    done(null, storyPoints);
  });
}

Sprint.updateStoryPoints = function(storyPoints, sprintId, done){
  var values = [storyPoints, storyPoints, sprintId];

  db.get().query('UPDATE sprints SET story_points = ?, remaining_story_points = ? WHERE sprints.id = ?', values, function(err, result){
    if (err) return done(err);
    done(null, result);
  });
}

Sprint.active = function(sprintId, done) {
  var values = [sprintId];

  db.get().query('SELECT end_date FROM sprints WHERE id = ?', values, function(err, result) {
    if (err) return done(err);

    var now = new Date();
    var endDate = new Date(result[0].end_date);

    if (now < endDate) {
      done(null, true);
    } else {
      done(null, false);
    }
  });
}

exports.Sprint = Sprint;
