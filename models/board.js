var db = require('../db');
var model = require('nodejs-model');

var Board = model("Board").attr('name', {
  validations: {
    presence: {
      message: 'Name is required.'
    }
  }
}).attr('creator', {
  validations: {
    presence: {
      message: 'Creator is required.'
    }
  }
});

Board.save = function(board, done) {

  //board creator will be the only member by default
  var values = [board.name(), board.creator()]

  db.get().query('INSERT INTO boards (name, creator)' +
                 ' VALUES (?, ?)', values,
                 function(err, result) {
                   if (err) return done(err);
                   done(null, result.insertId);
                 });

}

Board.populateUserBoards = function(userId, boardId, owner, done) {
  var values = [userId, boardId, owner];

  db.get().query('INSERT INTO userboards (user_id, board_id, owner)' +
                 ' VALUES (?, ?, ?)', values,
                 function(err, result) {
                   if (err) return done(err);
                   done(null, result.insertId);
                 });

}

Board.getBoardsForUser = function(userId, done) {
  var boards = [];
  db.get().query('SELECT board_id FROM userboards WHERE user_id = ?', [userId],
                function(err, results) {
                  results.forEach(function(result) {
                    getBoardFromId(result.board_id,
                      function(err, board) {
                        boards.push(board);
                      });
                  });
                  if (err) return done(err);
                  done(null, boards);
                });
}

Board.getBoardIdsForUser = function(userId, done) {
  var boards = [];
  db.get().query('SELECT board_id FROM userboards WHERE user_id = ?', [userId],
                function(err, results) {
                  var board_ids = [];
                  if (err) return done(err);
                  results.forEach(function(result) {
                    board_ids.push(result.board_id);
                  });
                  done(null, board_ids);
                });
}

Board.getBoardsForUser = function(userId, done) {
  db.get().query('SELECT boards.* FROM userboards INNER JOIN boards ON userboards.board_id = boards.id WHERE user_id = ?', [userId],
                function(err, results) {
                  if (err) return done(err);
                  done(null, results);
                });
}

Board.userAlreadyInvited = function(userId, boardId, done) {
  db.get().query('SELECT * FROM userboards WHERE user_id = ? AND board_id = ?', [userId, boardId],
                function(err, results) {
                  if (err) return done(err);
                  done(null, results[0]);
                });
}


Board.getBoardFromId = function(boardId, done) {
  db.get().query('SELECT * FROM boards WHERE id = ?', [boardId],
                function(err, results) {
                  if (err) return done(err);
                  done(null, results[0]);
                 });
}

Board.getInvitedUsers = function(boardId, done) {
  db.get().query('SELECT * FROM users INNER JOIN userboards ON userboards.user_id = users.id WHERE userboards.board_id = ? AND userboards.owner = 0', [boardId],
      function(err, results) {
        if (err) return done(err);
        done(null, results);
      });
}

Board.getBoardOwner = function(boardId, done) {
  db.get().query('SELECT * FROM users INNER JOIN userboards ON userboards.user_id = users.id WHERE userboards.board_id = ? AND userboards.owner = 1', [boardId],
      function(err, results) {
        if (err) return done(err);
        done(null, results[0]);
      });
}

Board.getActiveSprint = function(boardId, done) {
  var date = new Date();
  var values = [boardId, date];

  db.get().query('SELECT sprints.* FROM sprints INNER JOIN sprinttickets ON sprinttickets.sprint_id = sprints.id INNER JOIN tickets ON tickets.id = sprinttickets.ticket_id WHERE tickets.board_id = ? AND sprints.end_date > ? LIMIT 1', values, function(err, result) {
    if (err) return done(err);
    done(null, result[0]);
  });
}

Board.getPreviousSprints = function(boardId, done) {
  var date = new Date();
  var values = [boardId, date];

  db.get().query('SELECT sprints.* FROM sprints INNER JOIN sprinttickets ON sprinttickets.sprint_id = sprints.id INNER JOIN tickets ON tickets.id = sprinttickets.ticket_id WHERE tickets.board_id = ? AND sprints.end_date < ? GROUP BY sprints.id', values, function(err, result) {
    if (err) return done(err);
    done(null, result);
  });
}

exports.Board = Board;
