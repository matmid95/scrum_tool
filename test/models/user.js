var User = require('../../models/user').User;
var expect = require("chai").expect;

describe("User", function() {
  describe("create a new user", function() {
    it("has the expected attributes", function() {
      var expected_user_attrs = {
        name: 'foo',
        surname: 'bar',
        email: 'foo@bar.com',
        password: 'baz'
      };

      var user = User.create();
      user.name('foo');
      user.surname('bar');
      user.email('foo@bar.com');
      user.password('baz');

      console.log('expected: ' + JSON.stringify(expected_user_attrs));
      console.log('actual: ' + JSON.stringify(user.attrs));

      var expected = JSON.stringify(expected_user_attrs);
      var actual = JSON.stringify(user.attrs);

      expect(expected).to.equal(actual);
    });
  });
});
