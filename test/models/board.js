var Board = require('../../models/board').Board;
var expect = require("chai").expect;

describe("Board", function() {
  describe("create a new board", function() {
    it("has the expected attributes", function() {
      var expected_board_attrs = {
        name: 'foo',
        creator: 2,
      };

      var board = Board.create();
      board.name('foo');
      board.creator(2);

      console.log('expected: ' + JSON.stringify(expected_board_attrs));
      console.log('actual: ' + JSON.stringify(board.attrs));

      var expected = JSON.stringify(expected_board_attrs);
      var actual = JSON.stringify(board.attrs);

      expect(expected).to.equal(actual);
    });
  });
});
