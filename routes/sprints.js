var express = require('express');
var router = express.Router();
var async = require('async');

var auth = require('../helpers/authenticate');
var Board = require('../models/board').Board;
var User = require('../models/user').User;
var Ticket = require('../models/ticket').Ticket;
var Sprint = require('../models/sprint').Sprint;

/* GET sprint/id */
router.get('/:id', auth.checkAuth, function(req, res, next) {
  Sprint.getBoard(req.params.id, function(err, board) {
    Board.getInvitedUsers(board.id, function(err, invitedUsers) {
      Board.getBoardOwner(board.id, function(err, owner) {
        Sprint.getSprint(req.params.id, function(err, sprint) {
          Sprint.active(req.params.id, function(err, active) {
            Sprint.getTickets(req.params.id, function(err, tickets){
              formatTicketsArray(tickets, function(ticketsArrayToDo, ticketsArrayInProgress, ticketsArrayTesting, ticketsArrayDone){
                Sprint.calculateRemainingStoryPoints(req.params.id, function(err, remainingStoryPoints){
                  res.render('sprints/view', {
                    user: req.user,
                    board: board,
                    owner: owner,
                    invitedUsers: invitedUsers,
                    ticketsArrayToDo: ticketsArrayToDo,
                    ticketsArrayInProgress: ticketsArrayInProgress,
                    ticketsArrayTesting: ticketsArrayTesting,
                    ticketsArrayDone: ticketsArrayDone,
                    remainingStoryPoints: remainingStoryPoints,
                    active: active,
                    sprint: sprint
                  });
                });
              });
            });
          });
        });
      });
    });
  });
});

function formatTicketsArray(tickets, done) {
  var ticketsArrayToDo = [];
  var ticketsArrayInProgress = [];
  var ticketsArrayTesting = [];
  var ticketsArrayDone = [];

  tickets.forEach(function(ticket) {
    if (ticket.assignee) {
      User.getUserById(ticket.assignee, function(err, result){
        if (err) throw err;
        ticket.assignee = result.name + ' ' + result.surname;
      });
    } else {
      ticket.assignee = "None";
    }

    if (ticket.status_id === 1){
      ticketsArrayToDo.push({position: ticket.position, ticket: ticket});
    } else if (ticket.status_id === 2) {
      ticketsArrayInProgress.push({position: ticket.position, ticket: ticket});
    } else if (ticket.status_id === 3) {
      ticketsArrayTesting.push({position: ticket.position, ticket: ticket});
    } else {
      ticketsArrayDone.push({position: ticket.position, ticket: ticket});
    }
  });
  return done(ticketsArrayToDo, ticketsArrayInProgress, ticketsArrayTesting, ticketsArrayDone);
}

/* POST tickets/new */
router.post('/new', auth.checkAuth, function(req, res, next) {

  var title = req.body.sprintTitle;
  var length = req.body.sprintLength;
  var tickets = JSON.parse(req.body.tickets);

  var sprint = Sprint.create();
  var date = new Date();
  var endDate = new Date();
  endDate.setDate(date.getDate() + parseInt(length, 10));

  sprint.title(title);
  sprint.startDate(date);
  sprint.endDate(endDate);

  sprint.validate().then(function() {
    var sprintId;

    if (sprint.isValid) {
      Sprint.save(sprint, function(err, insertId){
        if (err) {
          throw err;
        }
        sprintId = insertId;
        addTicketsToSprint(tickets, sprintId, function() {
          Sprint.calculateStoryPoints(sprintId, function(err, storyPoints) {
            Sprint.updateStoryPoints(storyPoints, sprintId, function(err, storyPoints){
              res.send({redirect: '../sprints/' + sprintId});
            });
          });
        });
      });
    } else {
      req.flash('sprintError', 'Error creating the sprint');
    }
  });
});


function addTicketsToSprint(tickets, sprintId, done) {
  var ticketsAdded = 0;
  tickets.forEach(function(ticket){
    Sprint.populateSprintTickets(sprintId, ticket,
      function(err, insertId) {
        if (err) throw err;
        ticketsAdded++;
        // wait for all tickets to be added before continuing
        if (ticketsAdded === tickets.length){
          return done();
        }
      });
  });
}

module.exports = router;
