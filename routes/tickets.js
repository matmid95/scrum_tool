var express = require('express');
var router = express.Router();

var auth = require('../helpers/authenticate');
var Ticket = require('../models/ticket').Ticket;
var Board = require('../models/board').Board;
var User = require('../models/user').User;

/* GET tickets/:id */
router.get('/:id', auth.checkAuth, function(req, res, next) {
  Ticket.getTicketFromId(req.params.id, function(err, ticket) {
    Ticket.getAssignee(req.params.id, function(err, assignee) {
      Board.getInvitedUsers(ticket.board_id, function(err, invitedUsers) {
        Board.getBoardOwner(ticket.board_id, function(err, owner) {
          res.render('tickets/view', {
            user: req.user,
            invitedUsers: invitedUsers,
            owner: owner,
            assignee: assignee,
            ticket: ticket,
            assignSuccess: req.flash('assignUserSuccess')
          });
        });
      });
    });
  });
});

router.post('/rearrange', auth.checkAuth, function(req, res, next) {
  var ticketChanges = JSON.parse(req.body.ticketChanges);
  ticketChanges.forEach(function (ticketChange) {
    rearrangeTicket(ticketChange, function() {
    });
  });
});

router.post('/updateStatus', auth.checkAuth, function(req, res, next) {
  var ticketChanges = JSON.parse(req.body.ticketChanges);
  ticketChanges.forEach(function (ticketChange) {
    updateStatuses(ticketChange, function() {
    });
  });
  if(ticketChanges[0].ticketStatus === 4) {
    res.send({refresh: true});
  } else {
    res.send({refresh: false});
  }
});

router.post('/assign_user', auth.checkAuth, function(req, res, next) {
  var userId = req.body.assignee;
  var ticketId = req.body.ticketId;
  Ticket.assignUser(userId, ticketId, function(err, result) {
    if (err) throw err
    req.flash('assignUserSuccess', 'Successfully assigned user!');
    res.redirect('/tickets/' + ticketId);
  });
});

function rearrangeTicket(ticketChange, done) {
  Ticket.updatePosition(ticketChange.newTicketPos, ticketChange.ticketId, function(err, result) {
    if (err) throw err
    return done();
  });
}

function updateStatuses(ticketChange, done) {
  Ticket.updateStatus(ticketChange.ticketId, ticketChange.ticketStatus, function(err, result) {
    if (err) throw err
    return done();
  });
}

/* POST tickets/new */
router.post('/new', auth.checkAuth, function(req, res, next) {
  var name = req.body.name;
  var description = req.body.description;
  var storyPoints = req.body.story_points;
  var boardId = req.body.board_id;

  var ticket = Ticket.create();

  ticket.name(name);
  ticket.creator(req.user.id);
  ticket.description(description);
  ticket.storyPoints(storyPoints);
  ticket.boardId(boardId);


  ticket.validate().then(function() {

    if (ticket.isValid) {
      Ticket.save(ticket, function(err, insertId){
        if (err) {
          throw err;
        } else {
          // refresh boards page to show new board created
          res.redirect('/boards/' + boardId);
        }
      });
    } else {
    }
  });
});

module.exports = router;
