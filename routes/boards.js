var express = require('express');
var router = express.Router();
var async = require('async');

var auth = require('../helpers/authenticate');
var Board = require('../models/board').Board;
var User = require('../models/user').User;
var Ticket = require('../models/ticket').Ticket;

/* GET boards */
router.get('/', auth.checkAuth, function(req, res, next) {
  Board.getBoardsForUser(req.user.id, function(err, boards) {
    res.render('boards/list', {
      user: req.user,
      boards: boards
    });
  });
});

router.get('/:id', auth.checkAuth, function(req, res, next) {
  Board.getBoardFromId(req.params.id, function(err, board) {
    if (err) throw err;
    Board.getInvitedUsers(board.id, function(err, invitedUsers) {
      Board.getBoardOwner(board.id, function(err, owner) {
        Ticket.getTicketsForBoard(board.id, function(err, tickets){
          formatTicketsArray(tickets, function(ticketsArray, closedTicketsArray){
            Board.getActiveSprint(board.id, function(err, activeSprint){
              Board.getPreviousSprints(board.id, function(err, previousSprints){
                res.render('boards/view', {
                  user: req.user,
                  owner: owner,
                  invitedUsers: invitedUsers,
                  board: board,
                  tickets: tickets,
                  ticketsArray: ticketsArray,
                  activeSprint: activeSprint,
                  previousSprints: previousSprints,
                  closedTicketsArray: closedTicketsArray,
                  errors: req.flash('error')
                });
              });
            });
          });
        });
      });
    });
  });
});

function formatTicketsArray(tickets, done) {
  var ticketsArray = [];
  var closedTicketsArray = [];
  tickets.forEach(function(ticket) {
    if (ticket.assignee) {
      User.getUserById(ticket.assignee, function(err, result){
        if (err) throw err;
        ticket.assignee = result.name + ' ' + result.surname;
      });
    } else {
      ticket.assignee = "None";
    }
    if (ticket.status_id === 4 ) {
      closedTicketsArray.push({position: ticket.position, ticket: ticket});
    } else {
      ticketsArray.push({position: ticket.position, ticket: ticket});
    }
  });
  return done(ticketsArray, closedTicketsArray);
}

/* POST boards/new */
router.post('/new', auth.checkAuth, function(req, res, next) {
  var name = req.body.name;

  var board = Board.create();

  board.name(name);
  board.creator(req.user.id);

  board.validate().then(function() {
    var boardId;

    if (board.isValid) {
      Board.save(board, function(err, insertId){
        if (err) {
          throw err;
        }
        boardId = insertId;
        // owner is 1 as this user created the board
        var owner = 1;
        Board.populateUserBoards(req.user.id, boardId, owner,
          function(err, insertId) {
            if (err) {
              throw err;
            } else {
              // refresh boards page to show new board created
              res.redirect('/boards');
            }
          });
      });
    } else {
    }
  });
});

router.post('/:id/adduser', auth.checkAuth, function(req, res, next) {
  var email = req.body.email;

  User.getUserByEmail(email, function(err, user){
    if (err) throw err;
    if (typeof(user) == 'undefined') {
      req.flash('error', 'User with that email was not found');
      res.redirect('/boards/' + req.params.id);
    }
    else {
      Board.userAlreadyInvited(user.id, req.params.id, function(err, result) {
        if (err) throw err;
        if (typeof(result) !== 'undefined') {
          req.flash('error', 'This user has already been invited to this board');
          res.redirect('/boards/' + req.params.id);
        } else {
          // this user has been invited to the board so not the owner
          var owner = 0;
          Board.populateUserBoards(user.id, req.params.id, owner, function(err, result) {
            if (err) throw err;
            res.redirect('/boards/' + req.params.id);
          });
        }
      });
    }
  });
});


module.exports = router;
