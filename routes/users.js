var express = require('express');
var router = express.Router();

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

var User = require('../models/user').User;

var login_errors = [];

router.get('/login', function(req, res, next) {
  res.render('login', {errors: login_errors});
  login_errors = [];
});

// initialise strategy for user authentication
// define usernameField as email as passport
// expects a username field
passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password'
  },
  function(username, password, done) {
    var user = User.getUserByEmail(username, function(err, user) {
      if(err) throw err;

      if(typeof user == 'undefined'){
        login_errors.push('Invalid email');
        return done(null, false, {message: 'Invalid email'});
      }

      User.comparePassword(password, user.password, function(err, isMatch){
        if(err) throw err;

        if(isMatch){
          return done(null, user);
        } else {
          login_errors.push('Invalid password');
          return done(null, false, {message: 'Invalid password'});
        }
      });
    });
  })
);

passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  User.getUserById(id, function(err, user) {
    done(err, user);
  });
});

/* GET login */
/* POST login */
router.post('/login',
  passport.authenticate('local', {
    successRedirect: '/boards',
    failureRedirect: '/login',
    failureFlash: true
  }), function(req, res, next) {
    res.redirect('/boards');
});

router.get('/logout', function(req, res, next) {
  req.logout();
  res.redirect('/');
});

/* GET sign_up */
router.get('/sign_up', function(req, res, next) {
  res.render('sign_up', { errors: req.flash('error') });
});

/* POST sign_up (CREATE) */
router.post('/sign_up', function(req, res, next) {
  var name = req.body.name;
  var surname = req.body.surname;
  var email = req.body.email;
  var password = req.body.password;
  var confirmPassword = req.body.confirmPassword;

  var user = User.create();
  user.name(name);
  user.surname(surname);
  user.email(email);
  user.password(password);
  user.validate().then(function(){
    if (user.isValid) {
      User.save(user, function(err, insertId){
        if (err) throw err;
        req.flash('success', 'Successfully created account, please sign in below');
        res.render('login', { success_msg: req.flash('success') });
      });
    } else {
      //add errors for each field
      req.flash('error', user.errors.name);
      req.flash('error', user.errors.surname);
      req.flash('error', user.errors.email);
      req.flash('error', user.errors.password);

      res.render('sign_up', { errors: req.flash('error') });
    }
  });
});

module.exports = router;
